package ado

import (
	"context"
	"fmt"
	"net/url"
	"path"
	"strconv"
	"strings"
	"time"

	"github.com/k3a/html2text"
	"github.com/microsoft/azure-devops-go-api/azuredevops/workitemtracking"
)

// Representation of Azure DevOps Work Item
type WorkItem struct {
	Id                int
	Title             string
	Tags              []string
	Description       string
	Type              string
	CreatedDate       time.Time
	CreatedBy         User
	ChangedDate       time.Time
	ChangedBy         User
	TeamProject       string
	Priority          float64
	AreaPath          string
	IterationPath     string
	AssignedTo        User
	State             string
	StateChangeReason string
	StateChangedDate  time.Time
	CommentCount      float64
	RemainingWork     float64
}

// Print a brief overview of a work item
func PrintWorkItemBrief(wi WorkItem) {
	fmt.Println("Id:", wi.Id, "Type:", wi.Type, " ", wi.Title)
	fmt.Println("Assignee:", wi.AssignedTo.DisplayName)
	fmt.Println("Tags:", wi.Tags)
	fmt.Println()
	fmt.Println("Priority", wi.Priority)
	fmt.Println("State:", wi.State, "\t\t\t", "Area:", wi.AreaPath)
	fmt.Println("Reason:", wi.StateChangeReason, "\t", "Iteration:", wi.IterationPath)
	fmt.Println("Updated by", wi.ChangedBy.DisplayName, "on", wi.ChangedDate)
	fmt.Println()
	fmt.Println("Description")
	fmt.Println(html2text.HTML2Text(wi.Description))
}

// Print every detail of a work item
func PrintWorkItemDetailed(workitem WorkItem) {
	fmt.Println("Id:                    ", workitem.Id)
	fmt.Println("Title:                 ", workitem.Title)
	fmt.Println("Type:                  ", workitem.Type)
	fmt.Println("Tags:                  ", workitem.Tags)
	fmt.Println("Description:           ", workitem.Description)
	fmt.Println("Created:               ", workitem.CreatedDate)
	fmt.Println("Created By:            ", workitem.CreatedBy)
	fmt.Println("Changed:               ", workitem.ChangedDate)
	fmt.Println("Changed By:            ", workitem.ChangedBy)
	fmt.Println("Assigned To:           ", workitem.AssignedTo)
	fmt.Println("Priority:              ", workitem.Priority)
	fmt.Println("Iteration:             ", workitem.IterationPath)
	fmt.Println("State:                 ", workitem.State)
	fmt.Println("State changed:         ", workitem.StateChangedDate)
	fmt.Println("State change reason:   ", workitem.StateChangeReason)
	fmt.Println("Team Project:          ", workitem.TeamProject)
	fmt.Println("Comment Count:         ", workitem.CommentCount)
	fmt.Println("Remianing Work:        ", workitem.RemainingWork)
	fmt.Println("Description:")
	fmt.Println(html2text.HTML2Text(workitem.Description))
}

// Create a URL that points to the work item on the Azure DevOps website
func CreateUrlForWorkItem(wi WorkItem, organizationUrl string) (*url.URL, error) {
	wiUrl, err := url.Parse(organizationUrl)
	if err != nil {
		return nil, err
	}
	wiUrl.Path = path.Join(wiUrl.Path, wi.AreaPath, "_workitems", "edit", strconv.Itoa(wi.Id))
	return wiUrl, nil
}

// Receive and convert a work item from azure devops with the given ID
func GetWorkItemById(client workitemtracking.Client, context context.Context, id int) (*WorkItem, error) {
	workitemArgs := workitemtracking.GetWorkItemArgs{Id: &id}

	workitem, err := client.GetWorkItem(context, workitemArgs)
	if err != nil {
		return nil, err
	}

	return ConvertWorkItem(workitem)
}

// Convert the work item received from Azure DevOps into a WorkItem struct
func ConvertWorkItem(w *workitemtracking.WorkItem) (*WorkItem, error) {
	workitem := WorkItem{
		Id:                *w.Id,
		Title:             (*w.Fields)["System.Title"].(string),
		Description:       (*w.Fields)["System.Description"].(string),
		Type:              (*w.Fields)["System.WorkItemType"].(string),
		CreatedBy:         ConvertUser((*w.Fields)["System.CreatedBy"].(map[string]interface{})),
		ChangedBy:         ConvertUser((*w.Fields)["System.ChangedBy"].(map[string]interface{})),
		TeamProject:       (*w.Fields)["System.TeamProject"].(string),
		Priority:          (*w.Fields)["Microsoft.VSTS.Common.Priority"].(float64),
		AreaPath:          (*w.Fields)["System.AreaPath"].(string),
		IterationPath:     (*w.Fields)["System.IterationPath"].(string),
		State:             (*w.Fields)["System.State"].(string),
		StateChangeReason: (*w.Fields)["System.Reason"].(string),
		CommentCount:      (*w.Fields)["System.CommentCount"].(float64),
	}

	if value, ok := ((*w.Fields)["Microsoft.VSTS.Scheduling.RemainingWork"]).(float64); ok {
		workitem.RemainingWork = value
	}
	if value, ok := (*w.Fields)["System.AssignedTo"].(map[string]interface{}); ok {
		workitem.AssignedTo = ConvertUser(value)
	}
	if value, ok := (*w.Fields)["System.Tags"].(string); ok {
		workitem.Tags = strings.Split(value, "; ")
	}
	if createdDate, err := time.Parse(time.RFC3339, (*w.Fields)["System.CreatedDate"].(string)); err != nil {
		return nil, err
	} else {
		workitem.CreatedDate = createdDate
	}
	if changedDate, err := time.Parse(time.RFC3339, (*w.Fields)["System.ChangedDate"].(string)); err != nil {
		return nil, err
	} else {
		workitem.ChangedDate = changedDate
	}
	if stateChangedDate, err := time.Parse(time.RFC3339, (*w.Fields)["Microsoft.VSTS.Common.StateChangeDate"].(string)); err != nil {
		return nil, err
	} else {
		workitem.StateChangedDate = stateChangedDate
	}
	return &workitem, nil
}
