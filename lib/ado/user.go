package ado

// Representation of Azure DevOps User
type User struct {
	DisplayName string
	UniqueName  string
}

// Convert the received user from Azure DevOps API to User struct
func ConvertUser(user map[string]interface{}) User {
	return User{
		DisplayName: user["displayName"].(string),
		UniqueName:  user["uniqueName"].(string),
	}
}
