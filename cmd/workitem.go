package cmd

import (
	"context"
	"strconv"

	"gitlab.com/Lofter1/adgo/lib/ado"

	"github.com/microsoft/azure-devops-go-api/azuredevops"
	"github.com/microsoft/azure-devops-go-api/azuredevops/workitemtracking"
	"github.com/spf13/cobra"
	"github.com/spf13/viper"
)

// Flag name for detailed print
const detailedPrintFlagName = "detailed"

// Flag variable to enable detailed printing for work items
var detailedPrint bool

// Definition of workitem sub command
var workitemCmd = &cobra.Command{
	Use:     "workitem",
	Aliases: []string{"wi"},
	Short:   "Interact with ADO Workitems",
}

// Definition of workitem get sub command
var workitemGetCmd = &cobra.Command{
	Use:   "get [id]",
	Short: "Get workitem with given id",
	RunE:  runWorkItemGet,
	Args:  cobra.ExactArgs(1),
}

func init() {
	registerWorkItemCommands()
	initWorkItemFlags()
}

// Initialize flags for the workitem sub command and its sub commands
func initWorkItemFlags() {
	workitemGetCmd.Flags().BoolVar(&detailedPrint, detailedPrintFlagName, false, "Print detailed view of the work item")
}

// Register all commands for the workitem sub command
func registerWorkItemCommands() {
	rootCmd.AddCommand(workitemCmd)

	workitemCmd.AddCommand(workitemGetCmd)
}

// The starting point for workitem get
func runWorkItemGet(cmd *cobra.Command, args []string) error {
	ctx, client := getClient()

	workItemId, err := strconv.Atoi(args[0])
	if err != nil {
		return err
	}

	wi, err := ado.GetWorkItemById(client, ctx, workItemId)
	if err != nil {
		return err
	}

	if detailedPrint {
		ado.PrintWorkItemDetailed(*wi)
		return nil
	}

	ado.PrintWorkItemBrief(*wi)
	return nil
}

// Create the client objects for the Azure DevOps connection
func getClient() (context.Context, workitemtracking.Client) {
	connection := azuredevops.NewPatConnection(viper.GetString(organizationFlagName), viper.GetString(tokenFlagName))
	ctx := context.Background()
	client := azuredevops.NewClient(connection, viper.GetString(organizationFlagName))
	workItemClient := workitemtracking.ClientImpl{Client: *client}

	return ctx, &workItemClient
}
