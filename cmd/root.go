package cmd

import (
	"log"
	"os"

	"github.com/spf13/cobra"
	"github.com/spf13/viper"
)

const (
	// Flag Name for config option
	configFlagName = "config"
	//Flag Name for organization url option
	organizationFlagName = "organization"
	//Flag Name for access token option
	tokenFlagName = "token"
)

// Path to configuration file
var cfgFile string

// rootCmd represents the base command when called without any subcommands
var rootCmd = &cobra.Command{
	Use:   "adgo",
	Short: "Azure DevOps CLI written in Go",
	Long:  `Interact with Azure DevOps inside your command line`,
}

// Execute adds all child commands to the root command and sets flags appropriately.
// This is called by main.main(). It only needs to happen once to the rootCmd.
func Execute() {
	err := rootCmd.Execute()
	if err != nil {
		os.Exit(1)
	}
}

func init() {
	cobra.OnInitialize(initConfig)

	err := initRootFlags()
	if err != nil {
		log.Fatal(err)
	}
}

// Initialize all flags for the root command, used through the entire application
func initRootFlags() error {
	rootCmd.PersistentFlags().StringVar(&cfgFile, configFlagName, "", "config file")
	rootCmd.PersistentFlags().String(organizationFlagName, "", "ADO Organization URL")
	rootCmd.PersistentFlags().String(tokenFlagName, "", "ADO Personal Access Token")

	if err := viper.BindPFlag(organizationFlagName, rootCmd.PersistentFlags().Lookup(organizationFlagName)); err != nil {
		return err
	}
	if err := viper.BindPFlag(tokenFlagName, rootCmd.PersistentFlags().Lookup(tokenFlagName)); err != nil {
		return err
	}

	return nil
}

// initConfig reads in config file and ENV variables if set.
func initConfig() {
	if cfgFile != "" {
		// Use config file from the flag.
		viper.SetConfigFile(cfgFile)
	} else {
		viper.AddConfigPath(".")
		viper.SetConfigType("yaml")
		viper.SetConfigName(".adgo")
	}

	if err := viper.ReadInConfig(); err != nil {
		if err, ok := err.(viper.ConfigFileNotFoundError); !ok {
			log.Fatal(err)
		}
	}
}
